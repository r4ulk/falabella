# README #

This is a code challenge of Falabella Company using:

- Spring-Boot (Java 11), 
- Gradle build

### About the test 

##### Challenge 1 - Reductor Array

For two integer arrays, the comparator value is the total number of
elements in the first array such that there exists no integer in the
second array with an absolute difference less than or equal to d. Find
the comparator value.

For example there are two arrays a = [ 7, 5, 9], b = [ 13, 1, 4 ], and the
integer d = 3. The absolute difference of a[0] to b[0] = | 7 - 13 | = 6, b[1]
= | 7 - 1 | = 6, and b[2] = | 7 - 4 | = 3, to recap, the values are 6, 6, 3. In this
case, the absolute difference with b[2] is equal to d = 3, so this element
does not meet the criterion. A similar analysis of a[1] = 5 yields
absolute differences of 8, 4, 1 and of a[2] = 9 yields 4, 8, 5. The only
element of a that has an absolute difference with each element of
b that is always greater than d is element a[2], thus the comparator
value is 1.

##### How did I solve it?

    Using interface segregation and implementation of iterator for both arrays.

    Time complexity: O(n * m)

##### Challenge 2 - Shooting Bogeys

Multiple bogeys are coming down to destroy a military base. Each
bogey has a certain starting position above the ground level and speed
by which it is coming down. Arthur is in charge of the protection of
military base. Arthur has a gun which can shoot one bogey each
second. Arthur wants to know for how long he can protect the base.

For example, n = 3 bogeys are coming down, with starting positions
given by startingPos = [4, 3, 1] and speed = [3, 2, 1], then Arthur can
shoot the bogey with starting position 1 in the 1 second and the
bogey with starting position 4 in the 2 second, but cannot shoot
down the third bogey. So Arthur can protect the base for 2 seconds.

##### How did I solve it?

    Using interface segregation, OOP & implementation.
    For every array: int[] startingPosArray, int[] speedArray
    A Bogey Object is instantiated.
    Every new Bogey has your own secondsToDefeat attribute, 
    that means the amount seconds to defeat the military base.
    Checking the current position of a Bogey for every second, we can know if the military base was defeated.
    Otherwise Arthur still alive and defending the base.
   

### Running Test Cases ###

This application has three Unit Test Classes.

##### Challenge 1 - Reductor Array

```
com.falabella.codetest.reductorarray.ReductorArrayTest
```

##### Challenge 2 - Shooting Bogeys
```
com.falabella.codetest.shootingbogeys.BogeyTest
```
```
com.falabella.codetest.shootingbogeys.DefenseTest
```

to run All Unit Test Class, use the command-line:
```
gradle test
```

to run a Single Unit Test Class, use the command-line:
```
gradle test --tests {path-to-unit-test-class}
```
e.g.
```
gradle test --tests com.falabella.codetest.reductorarray.ReductorArrayTest
```

Credits
-
Raul Klumpp - raulklumpp@gmail.com

https://www.linkedin.com/in/raulk/
