package com.falabella.codetest.reductorarray.service;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 * @implNote - Interface Segregation for ReductorArray & comparatorValue
 */
public interface ReductorArray {

  int comparatorValue(int[] a, int[] b, int d);

}
