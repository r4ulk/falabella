package com.falabella.codetest.reductorarray.service.impl;

import com.falabella.codetest.reductorarray.service.ReductorArray;
import org.springframework.stereotype.Service;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 * @implNote - Implementation for ReductorArray & comparatorValue
 */
@Service
public class ReductorArrayImpl implements ReductorArray {

  /**
   * @implNote - comparatorValue Counts
   *                for each array value iterate from a[i] - b[i - b.length-1]
   * @param a - array a : first array to iterate
   * @param b - array b : second array to iterate
   * @param d - int d : the number to compare / locate
   * @return int - count of comparator where all element values > d
   */
  @Override
  public int comparatorValue(int[] a, int[] b, int d) {
    if (a.length == 0 || b.length == 0)
      return 0; // return zero always when one of arrays has length equals zero

    int count = 0;
    for (int number : a){
      if (!this.hasLessOrEquals(number, b, d))
        count++;
    }
    return count;
  }

  /**
   * @implNote - Helper method to iterate over b array
   *                and check if a[i] - b[i] is less or equals d
   * @param n - int n : The current value from first array index (i)
   * @param b - array b : Second array to iterate over values
   * @param d - int d : the number to compare/locate
   * @return boolean - return true when found value <= d
   */
  private boolean hasLessOrEquals(int n, int[] b, int d) {
    for (int number : b) {
      if (Math.abs(n - number) <= d)
        return Boolean.TRUE;
    }
    return Boolean.FALSE;
  }

}
