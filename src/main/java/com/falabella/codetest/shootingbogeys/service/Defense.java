package com.falabella.codetest.shootingbogeys.service;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 * @implNote - Interface Segregation for protectionTime implementations
 */
public interface Defense {

  int protectionTime(int[] startingPosArray, int[] speedArray);

}
