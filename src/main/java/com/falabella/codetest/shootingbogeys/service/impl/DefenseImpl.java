package com.falabella.codetest.shootingbogeys.service.impl;

import com.falabella.codetest.shootingbogeys.dto.Bogey;
import com.falabella.codetest.shootingbogeys.service.Defense;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 */
@Service
public class DefenseImpl implements Defense {

  /**
   * @implNote Iterates over the List of Bogeys, Check current position
   *               && decrease current position value
   *          Remove first element from List of Bogeys ( this element is the next to defeat the base )
   *
   * @param startingPosArray
   * @param speedArray
   * @return
   */
  @Override
  public int protectionTime(int[] startingPosArray, int[] speedArray) {
    int counter = 0;
    boolean defeated = Boolean.FALSE;

    var bogeys = this.buildBogeyList(startingPosArray, speedArray);

    while (defeated == Boolean.FALSE) {
      for (Bogey bogey : bogeys) {
        int currentPosition =  bogey.getCurrentPosition();
        if (currentPosition <= 0) {
          defeated = Boolean.TRUE;
          break;
        }
        bogey.setCurrentPosition(currentPosition-bogey.getSpeed());
      }
      if(!defeated) counter++;
      bogeys.remove(0); // killed first Bogey

      if(bogeys.isEmpty())  {
        defeated = Boolean.TRUE;
      }
    }
    return counter;
  }


  /**
   * @implNote Helper method to build a List of Bogey based on startingPosition and speed Arrays
   * @param startingPosArray - The int[] Array with statingPositions
   * @param speedArray - The int[] Array with speed values
   * @return List of Bogey
   */
  private List<Bogey> buildBogeyList(int[] startingPosArray, int[] speedArray) {
    List<Bogey> bogeys = new ArrayList<>();
    for(int i = 0; i < startingPosArray.length; i++) {
      bogeys.add(new Bogey(startingPosArray[i],  speedArray[i]));
    }
    return this.orderBySecondsToDefeat(bogeys);
  }

  /**
   * @implNote Helper method to order a List of Bogey based on attribute getSecondsToDefeat
   * @param bogeys - The List to iterate
   * @return ordered list of Bogeys
   */
  private List<Bogey> orderBySecondsToDefeat(List<Bogey> bogeys) {
    return bogeys.stream()
      .sorted(Comparator.comparingInt(Bogey::getSecondsToDefeat))
      .collect(Collectors.toList());
  }
  
}
