package com.falabella.codetest.shootingbogeys.dto;

/**
 * @author Raul Klumpp - raulklumpp@gmail.com
 * @implNote - OOP Class of Bogey
 */
public class Bogey {

  private int startPosition;
  private int currentPosition;
  private int speed;
  private int secondsToDefeat;

  public Bogey(int startPosition, int speed) {
    this.startPosition = startPosition;
    this.currentPosition = startPosition;
    this.speed = speed;
    this.setSecondsToDefeat();
  }

  public void setCurrentPosition(int currentPosition) {
    this.currentPosition = currentPosition;
  }

  public int getCurrentPosition() {
    return this.currentPosition;
  }

  public int getSpeed() {
    return this.speed;
  }

  private void setSecondsToDefeat() {
    this.secondsToDefeat = (int) Math.ceil(Double.valueOf(this.startPosition) / Double.valueOf(this.speed));
  }

  public int getSecondsToDefeat() {
    return this.secondsToDefeat;
  }

}
