package com.falabella.codetest.shootingbogeys;

import com.falabella.codetest.shootingbogeys.service.Defense;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DefenseTest {

  @Autowired
  private Defense defense;

  /**
   *  Should test ProtectionTime seconds with params:
   *          startPosition(4) & speed(1)
   *
   *      4 . (X) .  => killed at first second
   *      3 .  .  .
   *      2 .  .  .
   *      1 .  .  .
   *      0 .  .  .
   */
  @Test
  public void ShouldReturnProtectionTimeCaseOne() {
    int[] startingPos = {4};
    int[] speed = {1};

    int protectionTime = defense.protectionTime(startingPos, speed);

    assertThat(protectionTime).isEqualTo(1);
  }

  /**
   *  Should test ProtectionTime seconds with params:
   *          startPosition(4) & speed(1),
   *          startPosition(1) & speed(1)
   *
   *      4 . \O/ .  .  .  .                        .  .  .  .  .  .
   *      3 .  .  .  .  .  .                        . (X) .  .  .  . => killed at 2st second
   *      2 .  .  .  .  .  .                        .  .  .  .  .  .
   *      1 .  . (X) => killed at 1st second        .  .  .  .  .  .
   *      0 .  .  .  .  .  .                        .  .  .  .  .  .
   */
  @Test
  public void ShouldReturnProtectionTimeCaseTwo() {
    int[] startingPos = {4,1};
    int[] speed = {1,1};

    int protectionTime = defense.protectionTime(startingPos, speed);

    assertThat(protectionTime).isEqualTo(2);
  }

  /**
   *  Should test ProtectionTime seconds with params:
   *          startPosition(4) & speed(1),
   *          startPosition(1) & speed(1)
   *
   *      4 .  .  .  .  .  .                           .  .  .  .  .  .
   *      3 .  .  .  .  .  .                           .  .  .  .  .  .
   *      2 .  .  .  .  .  .                           .  .  .  .  .  .
   *      1 . \O/ . (X) => killed at 1st second        .  .  .  .  .  .
   *      0 .  .  .  .  .  .                           . \O/ .  .  .  .  => Military Base Defeated
   */
  @Test
  public void ShouldReturnProtectionTimeCaseThree() {
    int[] startingPos = {1,1};
    int[] speed = {1,1};

    int protectionTime = defense.protectionTime(startingPos, speed);

    assertThat(protectionTime).isEqualTo(1);
  }

  /**
   *  Should test ProtectionTime seconds with params:
   *          startPosition(4) & speed(3),
   *          startPosition(3) & speed(2),
   *          startPosition(1) & speed(1)
   *
   *      4 . \O/ .  .  .                         .  .  .  .  .                        .  .  .  .  .
   *      3 .  .  . \O/ .                         .  .  .  .  .                        .  .  .  .  .
   *      2 .  .  .  .  .                         .  .  .  .  .                        .  .  .  .  .
   *      1 .  . (X) => killed at 1st second      . \O/ . (X) => killed at 2st second  .  .  .  .  .
   *      0 .  .  .  .  .                         .  .  .  .  .                        . \O/ .  => Military Base Defeated
   */
  @Test
  public void ShouldReturnProtectionTimeCaseFour() {
    int[] startingPos = {4,1,3};
    int[] speed = {3,1,2};

    int protectionTime = defense.protectionTime(startingPos, speed);

    assertThat(protectionTime).isEqualTo(2);
  }

  /**
   *  Should test ProtectionTime seconds with params:
   *          startPosition(1) & speed(1),
   *          startPosition(2) & speed(1),
   *          startPosition(3) & speed(2),
   *          startPosition(4) & speed(3)
   *
   *      4 .  .  . \O/ .            .  .  .  .  .                      .  .  .  .  .
   *      3 .  . \O/ .  .            .  .  .  .  .                      .  .  .  .  .
   *      2 .  .  .  . \O/           .  .  .  .  .                      .  .  .  .  .
   *      1 . (X) => killed at 1st   .  . \O/\O/(X) => killed at 2st    .  .  .  .  .
   *      0 .  .  .  .  .            .  .  .  .  .                      . \O/\O/ .  .  => Military Base Defeated
   */
  @Test
  public void ShouldReturnProtectionTimeCaseFive() {
    int[] startingPos = {1,3,4,2};
    int[] speed = {1,2,3,1};

    int protectionTime = defense.protectionTime(startingPos, speed);

    assertThat(protectionTime).isEqualTo(2);
  }

}
