package com.falabella.codetest.shootingbogeys;

import com.falabella.codetest.shootingbogeys.dto.Bogey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BogeyTest {

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(4) & speed(3)
   *
   *      4 . \O/ .            .  .  .            .  .  .
   *      3 .  .  .            .  .  .            .  .  .
   *      2 .  .  .     =>     .  .  .            .  .  .
   *      1 .  .  .            . \O/ .     =>     .  .  .
   *      0 .  .  .            .  .  .            . \O/ .     ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseOne() {
    Bogey bogey = new Bogey(4, 3);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(2);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(1) & speed(1)
   *
   *      4 .  .  .             .  .  .
   *      3 .  .  .             .  .  .
   *      2 .  .  .     =>      .  .  .
   *      1 . \O/ .             .  .  .
   *      0 .  .  .             . \O/ .    ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseTwo() {
    Bogey bogey = new Bogey(1, 1);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(1);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(2) & speed(1)
   *
   *      4 .  .  .             .  .  .             .  .  .
   *      3 .  .  .             .  .  .             .  .  .
   *      2 . \O/ .     =>      .  .  .             .  .  .
   *      1 .  .  .             . \O/ .     =>      .  .  .
   *      0 .  .  .             .  .  .             . \O/ .      ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseThree() {
    Bogey bogey = new Bogey(2, 1);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(2);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(2) & speed(3)
   *
   *      4 .  .  .            .  .  .
   *      3 .  .  .            .  .  .
   *      2 . \O/ .    =>      .  .  .
   *      1 .  .  .            .  .  .
   *      0 .  .  .            . \O/ .    ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseFour() {
    Bogey bogey = new Bogey(2, 3);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(1);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(4) & speed(1)
   *
   *      4 . \O/ .             .  .  .             .  .  .             .  .  .             .  .  .
   *      3 .  .  .     =>      . \O/ .             .  .  .             .  .  .             .  .  .
   *      2 .  .  .             .  .  .     =>      . \O/ .             .  .  .             .  .  .
   *      1 .  .  .             .  .  .             .  .  .     =>      . \O/ .             .  .  .
   *      0 .  .  .             .  .  .             .  .  .             .  .  .     =>      . \O/ .   ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseFive() {
    Bogey bogey = new Bogey(4, 1);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(4);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(4) & speed(2)
   *
   *      4 . \O/ .            .  .  .            .  .  .
   *      3 .  .  .            .  .  .            .  .  .
   *      2 .  .  .    =>      . \O/ .            .  .  .
   *      1 .  .  .            .  .  .    =>      .  .  .
   *      0 .  .  .            .  .  .            . \O/ .   ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseSix() {
    Bogey bogey = new Bogey(4, 2);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(2);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(4) & speed(3)
   *
   *      4 . \O/ .            .  .  .             .  .  .
   *      3 .  .  .            .  .  .             .  .  .
   *      2 .  .  .    =>      .  .  .             .  .  .
   *      1 .  .  .            . \O/ .     =>      .  .  .
   *      0 .  .  .            .  .  .             . \O/ .      ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseSeven() {
    Bogey bogey = new Bogey(4, 3);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(2);
  }

  /**
   *  Should instantiate a new Bogey
   *      && calc seconds to defeat based on:
   *          startPosition(4) & speed(4)
   *
   *      4 . \O/ .            .  .  .
   *      3 .  .  .            .  .  .
   *      2 .  .  .    =>      .  .  .
   *      1 .  .  .            .  .  .
   *      0 .  .  .            . \O/ .    ---->   DEFEAT LINE
   */
  @Test
  public void ShouldCheckBogeySecondsToDefeatTestCaseEight() {
    Bogey bogey = new Bogey(4, 4);

    assertThat(bogey).isNotNull();
    assertThat(bogey.getSecondsToDefeat()).isEqualTo(1);
  }

}
