package com.falabella.codetest.reductorarray;

import com.falabella.codetest.reductorarray.service.ReductorArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReductorArrayTest {

  @Autowired
  private ReductorArray reductorArray;

  /**
   * Should Test:
   *     array a : [7,5,9]
   *     array b : [13,1,4]
   *       int d : 3
   * Expected result: 1
   */
  @Test
  public void ShouldReturnComparatorValueTestCaseOne() {
    int[] a = {7,5,9};
    int[] b = {13,1,4};
    int d = 3;

    int comparator = reductorArray.comparatorValue(a, b, d);

    assertThat(comparator).isEqualTo(1);
  }

  /**
   * Should Test:
   *     array a : [3,1,5]
   *     array b : [5,6,7]
   *       int d : 2
   * Expected result: 1
   */
  @Test
  public void ShouldReturnComparatorValueTestCaseTwo() {
    int[] a = {3,1,5};
    int[] b = {5,6,7};
    int d = 2;

    int comparator = reductorArray.comparatorValue(a, b, d);

    assertThat(comparator).isEqualTo(1);
  }

  /**
   * Assuming the scenario below is invalid
   * Should Test:
   *     array a : []
   *     array b : [5,6,7]
   *       int d : 2
   * Expected result: 0
   */
  @Test
  public void ShouldTestComparatorValueWithNoValuesInFirstArray() {
    int[] a = {}; // NO values in array a
    int[] b = {5,6,7};
    int d = 2;

    int comparator = reductorArray.comparatorValue(a, b, d);

    assertThat(comparator).isEqualTo(0);
  }

  /**
   * Assuming the scenario below is invalid
   * Should Test:
   *     array a : [3,1,5]
   *     array b : []
   *       int d : 2
   * Expected result: 0
   */
  @Test
  public void ShouldTestComparatorValueWithNoValuesInSecondArray() {
    int[] a = {3,1,5};
    int[] b = {}; // NO values in array b
    int d = 2;

    int comparator = reductorArray.comparatorValue(a, b, d);

    assertThat(comparator).isEqualTo(0);
  }

  /**
   * Should Test:
   *     array a : [3]
   *     array b : [5,6,7]
   *       int d : 2
   * Expected result: 0
   */
  @Test
  public void ShouldTestComparatorValueWithLessValuesInFirstArray() {
    int[] a = {3};
    int[] b = {5,6,7};
    int d = 2;

    int comparator = reductorArray.comparatorValue(a, b, d);

    assertThat(comparator).isEqualTo(0);
  }

  /**
   * Should Test:
   *     array a : [1]
   *     array b : [5,6,7]
   *       int d : 2
   * Expected result: 1
   */
  @Test
  public void ShouldTestComparatorValueWithLessValuesInFirstArrayScenarioTwo() {
    int[] a = {1};
    int[] b = {5,6,7};
    int d = 2;

    int comparator = reductorArray.comparatorValue(a, b, d);

    assertThat(comparator).isEqualTo(1);
  }

}
